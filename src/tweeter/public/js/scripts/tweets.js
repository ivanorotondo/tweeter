$(document).ready(function () {

  const picker = new tempusDominus.TempusDominus(document.getElementById('datetimepicker1'), { restrictions: { minDate: new Date() } });

  $('#tweetTextArea').on("input", function () {
    var maxlength = $(this).attr("maxlength");
    var currentLength = $(this).val().length;

    $('#tweetTextAreaCharacters').text(maxlength - currentLength + " characters left");
  });


  $('#sendButton').click(function () {
    if ($('#tweetTextArea').val() == "") {
      alert("The tweet cannot be empty.");
      return;
    }

    $('.overlay').show();
    $.ajax({
      url: "api/createTweet",
      type: "POST",
      data: {
        "text": $('#tweetTextArea').val()
      }
    }).done(function (response) {
      window.location.href = "/";
      $('.overlay').fadeOut();
    }).fail(function (data, textStatus, xhr) {
      alert("There has been an error loading your user, try again.");
      $('.overlay').fadeOut();
    });
  })


  $('#scheduleButton').click(function () {
    if ($('#tweetTextArea').val() == "") {
      alert("The tweet cannot be empty.");
      return;
    }

    if (picker.dates.picked.length > 0) {
      $('.overlay').show();
      $.ajax({
        url: "api/scheduleTweet",
        type: "POST",
        data: {
          "text": $('#tweetTextArea').val(),
          "scheduledDate": Math.floor(picker.dates.picked[0].getTime() / 1000),
        }
      }).done(function (response) {
        $('#scheduleModal').fadeOut();
        window.location.href = "/";
        $('.overlay').fadeOut();
      }).fail(function (data, textStatus, xhr) {
        alert("There has been an error loading your user, try again.");
        $('.overlay').fadeOut();
      });
    } else {
      alert("Pick up a valid schedule time");
    }
  })
})