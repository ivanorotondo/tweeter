
$(document).ready(function () {
  getUserInfo(getTweetsList);

  $('#publishedButton').click( function() {
    $('#tweetsTableHead').html('');
    $('#tweetsTableBody').html('');
    getUserInfo(getTweetsList);
  });

  $('#scheduledButton').click( function() {
    getScheduledTweets();
  });

  function getUserInfo(callback) {
    $('.overlay').show();
    $.ajax({
      "url": "api/getUserInfo",
      "method": "GET"
    }).done(function (response) {
      callback(JSON.parse(response).data.id);
    }).fail(function (data, textStatus, xhr) {
      alert("There has been an error loading your user, try again.")
      $('.overlay').fadeOut();
    });
  }

  function getTweetsList(userId) {
    $.ajax({
      url: "api/getTweetsList",
      method: "GET",
      dataType: 'json',
      contentType: 'application/json; charset=utf-8',
      data: {
        "userId": userId
      }
    }).done(function (response) {
      var response = JSON.parse(response);
      $('#tweetsTableHead').append($('<tr>')
      .append($('<td class="col-sm-3">').append('Id'))
      .append($('<td  class="col-sm-9">').append('Text')));

      $.each(response.data , function(index, tweet) {
        $('#tweetsTableBody').append($('<tr>')
        .append($('<td class="col-sm-3">').append(tweet.id))
        .append($('<td  class="col-sm-9">').append(tweet.text)));
      });
      
      $('.overlay').fadeOut();
    }).fail(function (data, textStatus, xhr) {
      alert("There has been an error loading your user, try again.")
      $('.overlay').fadeOut();
    });
  }

  function getScheduledTweets() {
    $('#tweetsTableHead').html('');
    $('#tweetsTableBody').html('');

    $('.overlay').show();
    $.ajax({
      "url": "api/getScheduledTweets",
      "method": "GET"
    }).done(function (response) {
      $('#tweetsTableHead').append($('<tr>')
      .append($('<td class="col-sm-3">').append('Date'))
      .append($('<td  class="col-sm-9">').append('Text')));

      $.each(response.data , function(index, tweet) {
        $('#tweetsTableBody').append($('<tr>')
        .append($('<td class="col-sm-3">').append(tweet.scheduled_date))
        .append($('<td  class="col-sm-9">').append(tweet.text)));
      });

      $('.overlay').fadeOut();
    }).fail(function (data, textStatus, xhr) {
      alert("There has been an error loading your user, try again.")
      $('.overlay').fadeOut();
    });
  }

})