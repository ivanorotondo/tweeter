<?php

namespace App\Http\Controllers\Api;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Providers\TwitterServiceProvider;
use App\Models\ScheduledTweet;
use Exception;
use App\Helpers\Helper;


class TwitterController extends Controller
{
  public function getUserInfo()
  {
    Helper::tweeterLog("api/getUserInfo call");
    $endpoint = '/users/me';
    $response = TwitterServiceProvider::call($endpoint, 'GET');

    if (!is_null($response)) {
      Helper::tweeterLog("api/getUserInfo call success - userId: " . json_decode($response)->data->id);
      return Response::json($response, 200);
    } else {
      return Response::json(['msg' => 'error'], 401);
    }
  }


  public function getTweetsList(Request $request)
  {
    $userId = $request->get('userId');
    Helper::tweeterLog("api/getTweetsList call - userId: " . $userId);

    $endpoint = '/users/' . $userId . '/tweets';
    $response = TwitterServiceProvider::call($endpoint, 'GET');

    if (!is_null($response)) {
      Helper::tweeterLog("api/getTweetsList call success");
      return Response::json($response, 200);
    } else {
      return Response::json(['msg' => 'error'], 401);
    }
  }


  public function createTweet(Request $request)
  {
    $text = $request->get('text');
    Helper::tweeterLog("api/createTweet call - text: " . $text);

    $endpoint = '/tweets';
    $response = TwitterServiceProvider::call($endpoint, 'POST', ["text" => $text]);

    if (!is_null($response)) {
      Helper::tweeterLog("api/createTweet call success");
      return Response::json(['data' => $response], 200);
    } else {
      return Response::json(['msg' => 'error'], 401);
    }
  }


  public function getScheduledTweets()
  {
    Helper::tweeterLog("api/getScheduledTweets");

    $allScheduledTweets = ScheduledTweet::get();
    return Response::json(['data' => $allScheduledTweets], 200);
  }


  public function scheduleTweet(Request $request)
  {
    try {
      $text = $request->get('text');
      $scheduledDate = $request->get('scheduledDate');

      Helper::tweeterLog("api/scheduleTweet call - text: " . $text . " | schedule date: " . $scheduledDate);

      ScheduledTweet::create([
        'text' => $text,
        'scheduled_date' => date('Y-m-d H:i:s', $scheduledDate)
      ]);
      return Response::json(['data' => ""], 200);
    } catch (Exception $e) {
      return Response::json(['msg' => 'error'], 401);
    }
  }
}
