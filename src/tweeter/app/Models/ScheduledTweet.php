<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScheduledTweet extends Model
{
    protected $table = 'scheduled_tweets';
    protected $fillable = [
        'text', 'scheduled_date'
    ];
}
