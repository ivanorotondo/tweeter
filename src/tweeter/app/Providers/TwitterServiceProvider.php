<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Response;
use App\Helpers\Helper;

class TwitterServiceProvider extends ServiceProvider
{

  static $twitterApiRoot = "https://api.twitter.com/2";

  public static function call($endpoint, $method, $data = null)
  {
    $url = self::$twitterApiRoot . $endpoint;
    $client = new Client();
    $options = [
      'headers' => [
        'Authorization' => TwitterServiceProvider::getOauthParams($url, $method)
      ]
    ];

    if ($data != null) {
      $options['json'] = $data;
    }

    try {
      $res = $client->request(
        $method,
        $url,
        $options
      );
      if (Helper::isHttpSuccess($res->getStatusCode())) {
        $content = $res->getBody()->getContents();
        return $content;
      } else {
        return Response::json(['msg' => 'error'], 401);
      }
    } catch (RequestException $e) {
      return Response::json(['msg' => 'error'], 401);
    }
  }

  private static function buildBaseString($baseURI, $method, $params)
  {
    $r = array();
    ksort($params);
    foreach ($params as $key => $value) {
      $r[] = "$key=" . rawurlencode($value);
    }
    return $method . "&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $r));
  }

  private static function buildAuthorizationHeader($oauth)
  {
    $r = 'OAuth ';
    $values = array();
    foreach ($oauth as $key => $value)
      $values[] = "$key=\"" . rawurlencode($value) . "\"";
    $r .= implode(', ', $values);
    return $r;
  }

  public static function getOauthParams($url, $method)
  {
    $oauth_access_token = env('TWITTER_OAUTH_ACCESS_TOKEN');
    $oauth_access_token_secret = env('TWITTER_OAUTH_ACCESS_TOKEN_SECRET');
    $consumer_key = env('TWITTER_OAUTH_CONSUMER_KEY');
    $consumer_secret = env('TWITTER_OAUTH_CONSUMER_SECRET');

    $oauth = array(
      'oauth_consumer_key' => $consumer_key,
      'oauth_nonce' => time(),
      'oauth_signature_method' => 'HMAC-SHA1',
      'oauth_token' => $oauth_access_token,
      'oauth_timestamp' => time(),
      'oauth_version' => '1.0'
    );

    $base_info = self::buildBaseString($url, $method, $oauth);
    $composite_key = rawurlencode($consumer_secret) . '&' . rawurlencode($oauth_access_token_secret);
    $oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
    $oauth['oauth_signature'] = $oauth_signature;
    return self::buildAuthorizationHeader($oauth);
  }
}
