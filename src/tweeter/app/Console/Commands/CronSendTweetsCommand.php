<?php

namespace App\Console\Commands;

use App\Models\ScheduledTweet;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use App\Providers\TwitterServiceProvider;


class CronSendTweetsCommand extends Command
{
    protected $signature = 'send:scheduled_tweets';

    protected $description = 'Sends scheduled tweets';


    public function handle()
    {
      $this->sendScheduledTweets();
    }

    private function sendScheduledTweets()
    {
      $scheduledTweets = ScheduledTweet::where('scheduled_date', '<=', Carbon::now('Europe/Rome'))->get();
      $endpoint = '/tweets';

      foreach ($scheduledTweets as $scheduledTweet) {
        TwitterServiceProvider::call($endpoint, 'POST', ["text" => $scheduledTweet->text]);
        $scheduledTweet->delete();
      }
    }
}