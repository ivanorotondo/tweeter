<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;
use DateTime;

class Helper
{
  public static function isHttpSuccess($status)
  {
    return 2 == (int)floor($status / 100);
  }

  public static function tweeterLog($log)
  {
    $logFileName = "tweeter.log";
    $dt = new DateTime();
    $text = $dt->format('d-m-Y H:i:s') . ' - ' . $log;
    try {
      $fileExists = Storage::exists($logFileName);
      if($fileExists) {
        Storage::append($logFileName, $text);
      } else {
        Storage::put($logFileName, $text);
      }
    } catch (\Exception $e) {
      dd($e);
    }
  }
}
