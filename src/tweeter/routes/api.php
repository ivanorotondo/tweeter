<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\TwitterController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::name('api')->group(function () {
  Route::get('/getUserInfo', [TwitterController::class, 'getUserInfo']);
  Route::get('/getTweetsList', [TwitterController::class, 'getTweetsList']);
  Route::post('/createTweet', [TwitterController::class, 'createTweet']);
  Route::post('/scheduleTweet', [TwitterController::class, 'scheduleTweet']);
  Route::get('/getScheduledTweets', [TwitterController::class, 'getScheduledTweets']);
});
