<nav class="navbar navbar-dark bg-dark" style="padding-left:20px; padding-right:20px;">
  <a class="navbar-brand" href="/">Tweeter</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">

    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="/">Tweets</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/createTweet">New tweet</a>
      </li>
    </ul>
    </div>
</nav>