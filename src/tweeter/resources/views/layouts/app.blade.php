<!DOCTYPE html>
<html lang="en">
@include('layouts.head')

<body>
  <div class="overlay">
    <div class="overlay__wrapper">
      <div class="overlay__spinner">
        <div class="spinner-border" role="status">
        </div>
      </div>
    </div>
  </div>
  @include('layouts.header')

  @yield('content')

  @include('layouts.scripts')
</body>

</html>