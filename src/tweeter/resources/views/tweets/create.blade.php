@extends('layouts.app')

@section('content')
<div class="container" style="margin-top:20px">
  <form>
    <div class="form-group">
      <label for="tweetTextArea">Tweet text</label>
      <textarea class="form-control" id="tweetTextArea" rows="5" maxlength="280"></textarea>
      <label id="tweetTextAreaCharacters">280 characters left</label>
    </div>

    <div class="btn-group" style="margin-top:20px">
      <button type="button" id="sendButton" class="btn btn-primary">Send now</button>
      <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown" aria-expanded="false">
      </button>
      <ul class="dropdown-menu">
        <li><button type="button" class="dropdown-item" data-bs-toggle="modal" data-bs-target="#scheduleModal">Schedule</button></li>
      </ul>
    </div>
  </form>

  <div class="modal fade" id="scheduleModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Tweet schedule</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class='input-group date' id='datetimepicker1' data-td-target-input='nearest' data-td-target-toggle='nearest'>
            <input id='datetimepicker1Input' type='text' class='form-control datetimepicker-input' data-td-target='#datetimepicker1' />
            <span class='input-group-text' data-td-target='#datetimepicker1' data-td-toggle='datetimepicker'>
              <span class='fas fa-calendar'></span>
            </span>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
          <button type="button" id="scheduleButton" class="btn btn-primary">Schedule</button>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('child-scripts')
<script src="/js/scripts/tweets.js"></script>
@endpush