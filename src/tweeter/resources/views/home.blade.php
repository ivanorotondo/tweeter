@extends('layouts.app')

@section('content')
  <div class="container" style="margin-top:20px">
    <div class="btn-group" role="group" style="width: 100%">
      <input type="radio" class="btn-check" name="btnradio" id="publishedButton" autocomplete="off" checked>
      <label class="btn btn-outline-dark" for="publishedButton">Published</label>

      <input type="radio" class="btn-check" name="btnradio" id="scheduledButton" autocomplete="off">
      <label class="btn btn-outline-dark" for="scheduledButton">Scheduled</label>
    </div>
    <table class="table table-striped" style="margin-top:20px">
      <thead id="tweetsTableHead">
      </thead>
      <tbody id="tweetsTableBody">
      </tbody>
    </table>
  </div>
@endsection

@push('child-scripts')
<script src="/js/scripts/home.js"></script>
@endpush